import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

Vue.config.productionTip = false

window.shared_data = {
  jeuxvideo: [
    {
      id: 0,
      nom: 'Hotline Miami',
      developpeur: "Dennaton Games",
      annee: 2012,
      url: 'https://fr.wikipedia.org/wiki/Hotline_Miami',
      description: "L'intrigue se déroule en 1989 à Miami. Un personnage inconnu, mais que les fans nomment Jacket en référence à son blouson, reçoit des appels qui lui ordonnent de commettre des crimes contre la mafia russe locale."
    },
    {
      id: 1,
      nom: 'Infamous 2',
      developpeur: "Sucker Punch Productions",
      annee: 2009,
      url: 'https://fr.wikipedia.org/wiki/Infamous_2',
      description: "À la suite de la destruction d'Empire City, Cole se tourne alors vers New Marais à la recherche de puissance pour vaincre la Bête"
    },
    {
      id: 2,
      nom: 'Borderlands 2',
      developpeur: "Gearbox Software",
      annee: 2012,
      url: 'https://fr.wikipedia.org/wiki/Borderlands_2',
      description: "L’histoire commence cinq ans après la fin du tout premier Borderlands, où une nouvelle Arche bien plus grande est découverte dans les tréfonds de la planète de Pandore."
    },
    {
      id: 3,
      nom: 'Borderlands 3',
      developpeur: "Gearbox Software",
      annee: 2019,
      url: 'https://fr.wikipedia.org/wiki/Borderlands_3',
      description: "Comme les précédents opus de la franchise, Borderlands 3 propose quatre personnages jouables, dits « chasseurs de l'arche »."
    },
    {
      id: 4,
      nom: 'Portal 2',
      developpeur: "Valve",
      annee: 2011,
      url: 'https://fr.wikipedia.org/wiki/Portal_2',
      description: "À la fin du premier opus, la protagoniste Chell détruit GLaDOS et échappe momentanément à l'établissement. Mais elle y est reconduite par une forme invisible à la voix robotique, identifiée plus tard par le scénariste Erik Wolpaw comme le « Robot Escorteur »."
    },
    {
      id: 5,
      nom: 'Ace Attorney',
      developpeur: "Capcom",
      annee: 2001,
      url: 'https://fr.wikipedia.org/wiki/Ace_Attorney',
      description: "Le joueur prendra le contrôle d'un avocat de la défense, devant innocenter son client, accusé d'avoir commis un meurtre. Durant les phases d'enquête, il faudra récolter des preuves, et obtenir des informations avec les différents témoins."
    },
    {
      id: 6,
      nom: 'Burnout Paradise',
      developpeur: "Criterion Games",
      annee: 2008,
      url: 'https://fr.wikipedia.org/wiki/Burnout_Paradise',
      description: "La jouabilité de Paradise est lancée dans la ville fictionnelle nommée « Paradise City », un monde ouvert dans lequel les joueurs courent dans de nombreux types de courses. Les joueurs peuvent également courir en-ligne."
    },
    {
      id: 7,
      nom: 'Mirror\'s Edge',
      developpeur: "DICE",
      annee: 2008,
      url: 'https://fr.wikipedia.org/wiki/Mirror%27s_Edge',
      description: "Le joueur incarne Faith, une des meilleures Messagères de la ville. Elle transporte illégalement des messages mais comme la rue est trop dangereuse pour elle, elle passe par les toits ce qui nécessite de nombreuses et dangereuses acrobaties. Depuis quelque temps la police, auparavant très occupée, semble pourchasser beaucoup plus souvent les Messagers."
    },
    {
      id: 8,
      nom: 'PAYDAY 2',
      developpeur: "Overkill Software",
      annee: 2013,
      url: 'https://fr.wikipedia.org/wiki/Payday_2',
      description: "Payday 2 reprend 4 personnages du premier opus : Dallas, Chains, Wolf et Hoxton. Ces personnalités sont incarnées par les joueurs lors des parties, mais ne définissent pas la (ou les) spécialisation de chaque joueur. Chaque joueur peut personnaliser son style de jeu, en achetant des compétences, des armes et leurs modifications, ainsi que des masques et leurs modifications au fur et à mesure qu'il gagne des niveaux (de la réputation) et de l'argent."
    },
    {
      id: 9,
      nom: 'The Last of Us',
      developpeur: "Naughty Dog",
      annee: 2013,
      url: 'https://fr.wikipedia.org/wiki/The_Last_of_Us',
      description: "Le titre prend place dans un univers post-apocalyptique après une pandémie provoquée par un champignon appelé le cordyceps. Les deux personnages principaux se nomment Joel (personnage incarné par le joueur) et Ellie, et doivent survivre ensemble lors de leur traversée du territoire américain."
    },
    {
      id: 10,
      nom: 'Yakuza 0',
      developpeur: "Ryu Ga Gotoku Studio",
      annee: 2017,
      url: 'https://fr.wikipedia.org/wiki/Yakuza_0',
      description: "Kazuma Kiryu est accusé d'un crime dont il est parfaitement innocent, il doit donc se blanchir de toute accusation. De son côté Gorô Majima, responsable d'un cabaret lucratif, fait face à un dilemme : tuer quelqu'un et retrouver son rang au sein des yakuzas ou faire fructifier son commerce."
    },
  ]
}


new Vue({
  render: h => h(App),
}).$mount('#app')
